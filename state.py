from transitions import Machine

# we read data each time (regardless of whether writing or not)
# to prevent potential buffering effects
class StateMachine(Machine):
    def __init__(self):
        states = ['wait', 'record']

        wait_t = {'source': 'wait',
                  'trigger': 'step',
                  'prepare': ['read_data', 'draw_user'],
                  'conditions': 'wait_for_space',
                  'after': ['text_to_record', 'reset_space'],
                  'dest': 'record'}

        record_t = {'source': 'record',
                    'trigger': 'step',
                    'prepare': ['read_data', 'write_data', 'draw_user'],
                    'conditions': 'wait_for_space',
                    'after': ['text_to_pause', 'increment', 'reset_space'],
                    'dest': 'wait'}

        transitions = [wait_t, record_t]
        Machine.__init__(self, states=states, transitions=transitions, initial='wait')
