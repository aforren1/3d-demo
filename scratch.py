import matplotlib.pyplot as plt
import numpy as np

with open('trial0_indices34.csv') as f:
    data = np.loadtxt(f, delimiter=',')

plt.plot(data - np.median(data, axis=0))

plt.show()
