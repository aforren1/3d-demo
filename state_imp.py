import sys

import numpy as np
from direct.gui.OnscreenImage import OnscreenImage
from direct.gui.OnscreenText import OnscreenText
from direct.gui.DirectGui import DirectEntry, DirectButton
from direct.showbase.ShowBase import ShowBase
from direct.task.TaskManagerGlobal import taskMgr
from panda3d.core import (AntialiasAttrib, PointLight, Spotlight, TextNode,
                          TransparencyAttrib, BitMask32)

from state import StateMachine
from toon.input import MultiprocessInput
from toon.input.hand import Hand


class Experiment(StateMachine, ShowBase):
    def __init__(self):
        super(Experiment, self).__init__()
        self.highlighted_indices = []

        self.render.setAntialias(AntialiasAttrib.MMultisample)
        self.render.setShaderAuto()
        self.setFrameRateMeter(True)
        self.disableMouse()
        self.setup_lights()
        self.setup_camera()
        self.load_models()

        self.text = OnscreenText(text='Not recording.', pos=(-0.8, 0.8),
                                 scale=0.08, fg=(1, 1, 1, 1),
                                 bg=(0, 0, 0, 1), frame=(0.2, 0.2, 0.8, 1),
                                 align=TextNode.ACenter)
        self.text.reparentTo(self.aspect2d)

        self.entry = DirectEntry(text='', scale=0.08, command=self.change_visible,
                                 initialText="", numLines=1, focus=1,
                                 focusInCommand=self.clearTxt, pos=(
                                     -0.8, 0, 0.65),
                                 text_bg=(0, 0, 0, 1), text_fg=(1, 1, 1, 1),
                                 text_align=TextNode.ACenter, pad=(0, 0),
                                 frameColor=(0, 0, 0, 1))
        self.entry.reparentTo(self.aspect2d)

        self.button = DirectButton(
            text='center', scale=0.05, command=self.zero, pos=(-0.95, 0, 0.95))

        taskMgr.add(self.update_state, 'update_state')
        self.accept('space', self.space_on)
        self.accept('escape', sys.exit)

        self.counter = 0  # for file naming
        self.space = False
        self.device = MultiprocessInput(Hand)
        self.data = None
        self.throwaway = 0
        self.intercepts = np.array((0.0692524369701519, -0.0692524369701519, -0.277009747880608,
                                    -0.064854057909167, 0.064854057909167, 0.259416231636668, -0.0491464674633581,
                                    0.0491464674633581, 0.196585869853433, 0.148444417305285, -0.148444417305285,
                                    -0.59377766922114, -0.132224666927635, 0.132224666927635, 0.528898667710541))
        self.slopes = np.array((2.97859943957643, 2.97859943957643, 11.9143977583057, 5.40450482576391,
                                5.40450482576391, 21.6180193030556, 4.27360586637896, 4.27360586637896,
                                17.0944234655158, 8.7320245473697, 8.7320245473697, 34.9280981894788,
                                3.67290741465653, 3.67290741465653, 14.6916296586261))

    def change_visible(self, txt):
        if self.state is not 'record':
            self.highlighted_indices = txt.split(',')
            self.highlighted_indices = [int(x)
                                        for x in self.highlighted_indices]
            print(self.highlighted_indices)
            self.set_visible_players()

    def set_visible_players(self):
        for i in range(5):
            if i in self.highlighted_indices:
                self.players[i].setAlphaScale(1.0)
                self.player_axes[i].show()
                self.players[i].show(BitMask32.bit(0))
            else:
                self.players[i].setAlphaScale(0.5)
                self.player_axes[i].hide()  # completely remove axes
                self.players[i].hide(BitMask32.bit(0))  # only remove shadow

    def clearTxt(self):
        self.entry.enterText('')

    def setup_lights(self):
        pl = PointLight('pl')
        pl.setColor((1, 1, 1, 1))
        plNP = self.render.attachNewNode(pl)
        plNP.setPos(-1, -1, 1)
        self.render.setLight(plNP)

        pos = [[[0, 0, 6], [0, 0, -2]],
               [[0, -6, 0], [0, 2, 0]],
               [[-6, 0, 0], [2, 0, 0]]]
        self.spotlights = list()
        for counter, value in enumerate(pos):
            dl = Spotlight('dl')
            dl.setColor((1, 1, 1, 1))
            self.spotlights.append(self.render.attachNewNode(dl))
            self.spotlights[counter].setPos(*value[0])
            self.spotlights[counter].lookAt(*value[1])
            self.spotlights[counter].node().setShadowCaster(True)
            self.spotlights[counter].node().setCameraMask(BitMask32.bit(0))
            self.render.setLight(self.spotlights[counter])

    def setup_camera(self):
        self.cam.setPos(-4, -8, 4)
        self.cam.lookAt(0, 0, 0)

    def load_models(self):
        self.back_model = self.loader.loadModel('models/back')
        self.target = self.loader.loadModel('models/target')

        self.back_model.setScale(3, 3, 3)
        self.back_model.reparentTo(self.render)

        self.anchors = list()  # just another marker for the zeroed position
        self.players = list()
        self.player_offsets = [[-1.0, -1.2, -0.2], [-0.6, -0.4, -0.1], [0, 0, 0],
                               [0.6, -0.3, -0.1], [1.0, -0.8, -0.2]]
        p_col = [[228, 26, 28], [55, 126, 184], [
            77, 175, 74], [152, 78, 163], [255, 127, 0]]

        self.player_axes = list()
        for counter, value in enumerate(self.player_offsets):
            self.players.append(self.loader.loadModel('models/player'))
            self.player_axes.append(self.loader.loadModel('models/axes'))
            self.anchors.append(self.loader.loadModel('models/target'))
            self.players[counter].reparentTo(self.render)
            self.players[counter].setPos(*value)
            self.players[counter].setScale(0.1, 0.1, 0.1)
            self.players[counter].setColorScale(
                p_col[counter][0]/255, p_col[counter][1]/255, p_col[counter][2]/255, 1)
            self.players[counter].setTransparency(TransparencyAttrib.MAlpha)
            self.player_axes[counter].reparentTo(self.render)
            self.player_axes[counter].setPos(*value)
            self.anchors[counter].reparentTo(self.render)
            self.anchors[counter].setPos(*value)
            self.anchors[counter].setScale(0.05, 0.05, 0.05)
            self.anchors[counter].setColorScale(0, 0, 0, 1)
            self.anchors[counter].hide(BitMask32.bit(0))
        self.set_visible_players()

        self.cam2dp.node().getDisplayRegion(0).setSort(-20)
        OnscreenImage(parent=self.cam2dp, image='models/background.jpg')

        self.text = OnscreenText(text='Not recording.', pos=(-0.8, 0.8),
                                 scale=0.08, fg=(1, 1, 1, 1),
                                 bg=(0, 0, 0, 1), frame=(0.2, 0.2, 0.8, 1),
                                 align=TextNode.ACenter)
        self.text.reparentTo(self.aspect2d)

    def update_state(self, task):
        self.step()
        return task.cont

    def text_to_pause(self):
        self.text.setText('Not recording.')

    def text_to_record(self):
        self.text.setText('Recording data.')

    def space_on(self):
        self.space = True

    def wait_for_space(self):
        return self.space

    def reset_space(self):
        self.space = False

    def read_data(self):
        ts, data = self.device.read()
        if ts is not None:
            # self.timestamp = ts
            #data *= self.slopes + self.intercepts  # convert to forces
            data *= 2
            if self.data is None:
                self.med_data = np.median(data, axis=0)
            self.data = data

    def draw_user(self):
        """Set position for each player (reading - median + offset)"""
        if self.data is not None:
            k = 0
            r = 0
            for i, p in zip(self.players, self.player_offsets):
                i.setPos(-self.data[-1, k] + p[0] + self.med_data[k],
                         self.data[-1, k + 1] + p[1] - self.med_data[k + 1],
                         -self.data[-1, k + 2] + p[2] + self.med_data[k + 2])
                if i.getColorScale()[3] > 0.5 and self.throwaway > 10:
                    self.throwaway = 0
                    print([r, -self.data[-1, k] + self.med_data[k], self.data[-1, k+1] -
                           self.med_data[k+1], -self.data[-1, k+2] + self.med_data[k+2]])
                r += 1
                k += 3
            self.throwaway += 1

    def write_data(self):
        with open('trial' + str(self.counter) + '_indices' + ''.join(str(x) for x in self.highlighted_indices) + '.csv', 'ab') as f:
            np.savetxt(f, self.data, delimiter=',', fmt='%f')

    def increment(self):
        self.counter += 1

    def zero(self):
        if self.data is not None:
            self.med_data = np.median(self.data, axis=0)
