import numpy as np
from state_imp import Experiment

if __name__ == '__main__':
    experiment = Experiment()
        
    with experiment.device:
        experiment.run()
